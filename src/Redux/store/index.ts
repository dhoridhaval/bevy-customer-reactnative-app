import createSagaMiddleware from 'redux-saga';
import { rootSaga } from '../saga';
import rootReducer from '../reducer/rootReducer';
import persistStore from 'redux-persist/es/persistStore';
import { configureStore } from '@reduxjs/toolkit';

const sagaMiddleware = createSagaMiddleware();

export const store = configureStore({
  reducer: rootReducer,
  middleware: [sagaMiddleware],
});

sagaMiddleware.run(rootSaga);

export const persistor = persistStore(store);

export default store;
