import { AddUser } from '../../Interfaces';
import * as TYPES from '../types';

export const addUserAction = () => ({
  type: TYPES['ADD_USER_ACTION'],
});

export const addUserSuccessAction = (payload: AddUser) => ({
  type: TYPES['ADD_USER_SUCCESS_ACTION'],
  payload,
});
