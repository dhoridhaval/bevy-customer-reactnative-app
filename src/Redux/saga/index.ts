import { all, fork } from 'redux-saga/effects';
import { AddUserSaga } from './user.saga';

export function* rootSaga() {
  yield all([fork(AddUserSaga)]);
}
