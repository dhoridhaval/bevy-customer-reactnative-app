import { AddUserAction } from '../../Interfaces';
import * as Types from '../types';

const intialState = {
  data: [],
};

export const addUserReducer = (state = intialState, action: AddUserAction) => {
  const { type, payload } = action;
  switch (type) {
    case Types['ADD_USER_ACTION']:
      return {
        ...state,
      };
    case Types['ADD_USER_SUCCESS_ACTION']:
      return {
        ...state,
        data: payload,
      };
    default:
      return state;
  }
};
