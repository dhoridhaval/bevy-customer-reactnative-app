import { combineReducers } from 'redux';
import { addUserReducer } from './addUserReducer';

const rootReducer = combineReducers({
  user: addUserReducer,
});

export default rootReducer;
