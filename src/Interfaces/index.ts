import * as TYPES from '../Redux/types';

// Actions Creators
export interface AddUser {
  id: number;
  name: string;
}

// Action types for reducer
type UserActionTypes = typeof TYPES['ADD_USER_ACTION'];

export interface AddUserAction {
  type: UserActionTypes;
  payload?: AddUser | string;
}
