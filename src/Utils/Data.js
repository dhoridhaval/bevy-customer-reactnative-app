const Data = [
  {
    imgUrl: 'https://i.ibb.co/hYjK44F/anise-aroma-art-bazaar-277253.jpg',
  },
  {
    imgUrl: 'https://i.ibb.co/JtS24qP/food-inside-bowl-1854037.jpg',
  },
  {
    imgUrl:
      'https://i.ibb.co/JxykVBt/flat-lay-photography-of-vegetable-salad-on-plate-1640777.jpg',
  },
];

export default Data;

export const Dummydata = [
  {
    title: 'Promo',
    uri: require('../Assets/Images/PromoCard.png'),
  },
  {
    title: 'New',
    uri: require('../Assets/Images/PromoCard2.png'),
  },
  {
    title: 'Acqua',
    uri: require('../Assets/Images/PromoCard3.png'),
  },
  {
    title: 'Birre',
    uri: require('../Assets/Images/PromoCard4.png'),
  },
  {
    title: 'Bevande Gasate',
    uri: require('../Assets/Images/PromoCard5.png'),
  },
  {
    title: 'Caffé',
    uri: require('../Assets/Images/PromoCard6.png'),
  },
  {
    title: 'Succhi',
    uri: require('../Assets/Images/PromoCard.png'),
  },
  {
    title: 'Té Freddi',
    uri: require('../Assets/Images/truckIcon.png'),
  },
  {
    title: 'Energy & Sport Drinks',
    uri: require('../Assets/Images/PromoCard2.png'),
  },
  {
    title: 'Dietro L’angolo',
    uri: require('../Assets/Images/PromoCard4.png'),
  },
];
