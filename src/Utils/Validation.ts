import * as Yup from 'yup';

const LoginValidation = Yup.object({
  email: Yup.string().email('Invalid email address').required('Required'),
  password: Yup.string()
    .min(5, 'Password must be at least 5 characters')
    .required('Required'),
});

export default LoginValidation;
