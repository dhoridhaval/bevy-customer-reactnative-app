const Images = {
  CANCELIMAGE: require('../Assets/Images/closeIcon.png'),
  EUROSIGN: require('../Assets/Images/eurosign.png'),
  BEVYLOGO: require('../Assets/Images/Bevylogo.png'),
  LOCATIONICON: require('../Assets/Images/locationIcon.png'),
  EDITICON: require('../Assets/Images/editIcon.png'),
  GLASSICON: require('../Assets/Images/glassIcon.png'),
  TRUCKICON: require('../Assets/Images/truckIcon.png'),
  HANDPICKICON: require('../Assets/Images/handPickIcon.png'),
  PROMOCARD: require('../Assets/Images/PromoCard.png'),
};
export default Images;
