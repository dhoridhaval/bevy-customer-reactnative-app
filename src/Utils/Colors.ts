const Colors = {
  Black: '#000000',
  White: '#fff',
  Blue: '#0000ff',
  Red: '#ff0000',
  Gray: '#959595',
  DeepBlue: '#0033A1',
  acqua: '#18F0D5',
  GreyG: '#EBEBEB'
};

export default Colors;
