import React from 'react';
import { Image, Text, View } from 'react-native';
import { Images } from '../../Utils';
import style from './styles';

const DiscountsBanner = () => {
  return (
    <View style={style.container}>
      <View style={style.titleContainer}>
        <Text style={style.title}>10</Text>
        <Image source={Images.EUROSIGN} style={style.euroIcon} />
        <Text style={style.subtitle}>di scronto sul tuo primo ordine.</Text>
      </View>
      <View style={style.titleContainer}>
        <View style={style.buttonView}>
          <Text style={style.buttonTitle}>BEVY10</Text>
        </View>
        <Image source={Images.CANCELIMAGE} style={style.cancelIcon} />
      </View>
    </View>
  );
};

export default DiscountsBanner;
