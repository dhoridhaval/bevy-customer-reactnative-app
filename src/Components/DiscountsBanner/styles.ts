import { StyleSheet } from 'react-native';
import { Colors } from '../../Utils';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: '100%',
    height: 48,
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 14,
    backgroundColor: Colors.DeepBlue,
  },
  titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  title: {
    fontSize: 22,
    fontWeight: '800',
    fontFamily: 'kanit',
    fontStyle: 'italic',
    color: Colors.acqua,
  },
  subtitle: {
    fontSize: 12,
    marginLeft: 6,
    fontWeight: '500',
    fontFamily: 'kanit',
    marginTop: 4,
    color: Colors.acqua,
  },
  buttonView: {
    marginRight: 17,
    backgroundColor: Colors.acqua,
    color: Colors.DeepBlue,
  },
  buttonTitle: {
    color: Colors.DeepBlue,
    fontSize: 18,
    fontWeight: '800',
    fontStyle: 'italic',
    paddingVertical: 6,
    paddingHorizontal: 16,
  },
  cancelIcon: {
    tintColor: Colors.White,
    height: 14,
    width: 14,
  },
  euroIcon: {
    height: 16,
    width: 16,
  },
});
export default styles;
