import React from 'react';
import { Image, Text, View } from 'react-native';
import { Images } from '../../Utils';
import styles from './styles';

const HowToUse = () => {
  return (
    <View style={styles.outerContainer}>
      <View style={styles.container}>
        <Image source={Images.GLASSICON} style={styles.icons} />
        <View style={styles.innerBox}>
          <Text style={styles.lightText}>Passo 1</Text>
          <Text style={styles.text}>Seleziona le tue bevande</Text>
        </View>
      </View>

      <View style={styles.container}>
        <Image source={Images.HANDPICKICON} style={styles.icons} />
        <View style={styles.innerBox}>
          <Text style={styles.lightText}>Passo 2</Text>
          <Text style={styles.text}>Scegli quando riceverle</Text>
        </View>
      </View>

      <View style={styles.container}>
        <Image source={Images.TRUCKICON} style={styles.icons} />
        <View style={styles.innerBox}>
          <Text style={styles.lightText}>Passo 3</Text>
          <Text style={styles.text}>Arriviamo senza costi di consegna</Text>
        </View>
      </View>
    </View>
  );
};

export default HowToUse;
