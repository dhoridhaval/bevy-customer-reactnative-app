import {StyleSheet} from 'react-native';
import {Colors} from '../../Utils';

const styles = StyleSheet.create({
  outerContainer: {
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    backgroundColor: 'white',
    height: 258,
    top: -25,
    borderRadius: 20,
    borderWidth: 1.5,
    borderColor: Colors.GreyG,
    marginHorizontal: 17,
    padding: 15,
  },
  container: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  icons: {
    height: 61,
    width: 61,
    backgroundColor: Colors.GreyG,
    borderRadius: 60,
  },
  innerBox: {
    marginLeft: 14,
  },
  text: {
    fontSize: 15,
    fontWeight: '600',
    color: '#000000',
  },
  lightText: {
    fontSize: 13,
    fontFamily: 'UCity',
    fontWeight: '400',
    color: '#6A6A6A',
  },
});
export default styles;
