import React from 'react'
import { View, StyleSheet, Dimensions, Image } from "react-native"

export const SLIDER_WIDTH = Dimensions.get('window').width + 1
export const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 1)

const CarouselItem = ({ item, index }:any) => {
  return (
    <View style={styles.container} key={index}>
      <Image
        source={{ uri: item.imgUrl }}
        style={styles.image}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    width: ITEM_WIDTH,
    elevation: 7,
  },
  image: {
    width: "100%",
    height: 222,
  },
})

export default CarouselItem
