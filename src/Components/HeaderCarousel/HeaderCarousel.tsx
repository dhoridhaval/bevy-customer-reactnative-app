import React from "react";
import { View } from "react-native";
import Carousel from "react-native-snap-carousel";
import Data from "../../Utils/Data";
import CarouselItem, { ITEM_WIDTH, SLIDER_WIDTH } from "./CarouselItem";

const HeaderCarousel = () => {
  const [index, setIndex] = React.useState(0);
  const isCarousel = React.useRef(null);

  return (
    <View>
      <Carousel
        layout="tinder"
        layoutCardOffset={9}
        ref={isCarousel}
        data={Data}
        autoplay={5}
        autoplayDelay={5}
        renderItem={CarouselItem}
        sliderWidth={SLIDER_WIDTH}
        itemWidth={ITEM_WIDTH}
        onSnapToItem={(index) => setIndex(index)}
        useScrollView={true}
      />
    </View>
  );
};

export default HeaderCarousel;