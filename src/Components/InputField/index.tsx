import React from 'react';
import { Text, TextInput, View } from 'react-native';
import { Colors } from '../../Utils';
import styles from './styles';

interface InputFieldProps {
  placeholder: string;
  value?: string;
  secureTextEntry?: boolean;
  onChangeText: (text: string) => void;
  customStyle?: object;
  error?: boolean;
  errorText?: string;
}

const InputField: React.FC<InputFieldProps> = ({
  placeholder,
  value,
  secureTextEntry,
  onChangeText,
  customStyle,
  error,
  errorText,
}) => {
  return (
    <View>
      <View style={styles.sectionstyle}>
        <TextInput
          editable
          value={value}
          onChangeText={onChangeText}
          secureTextEntry={secureTextEntry}
          placeholder={placeholder}
          placeholderTextColor={Colors.Black}
          style={[
            customStyle,
            styles.inputField,
            { borderColor: error ? Colors.Red : 'transparent' },
          ]}
        />
      </View>
      {error && (
        <View style={styles.errorContainer}>
          <Text style={styles.errorText}>{errorText || 'Error Occured'}</Text>
        </View>
      )}
    </View>
  );
};

export default InputField;
