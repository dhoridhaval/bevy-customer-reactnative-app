import { StyleSheet } from 'react-native';
import { Colors } from '../../Utils';

const styles = StyleSheet.create({
  inputField: {
    flex: 1,
    backgroundColor: Colors.White,
    width: '100%',
    paddingHorizontal: 10,
  },
  sectionstyle: {
    paddingHorizontal: 10,
    flexDirection: 'row',
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    marginBottom: 10,
  },
  errorContainer: {
    alignSelf: 'flex-end',
    marginRight: 5,
  },
  errorText: {
    color: Colors.Red,
    fontSize: 12,
  },
});
export default styles;
