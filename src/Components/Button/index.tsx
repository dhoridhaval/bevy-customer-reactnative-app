import React from 'react';
import { Text, TouchableOpacity, ActivityIndicator } from 'react-native';
import styles from './styles';

interface ButtonProps {
  label: string;
  onPress?: () => void;
  customStyle?: object;
  disabled?: boolean;
  isLoading?: boolean;
}

const Button: React.FC<ButtonProps> = ({
  label,
  onPress,
  customStyle,
  disabled,
  isLoading,
}) => {
  return (
    <TouchableOpacity
      disabled={disabled}
      onPress={onPress}
      style={[styles.buttonContainer, customStyle]}>
      {isLoading ? <ActivityIndicator size="small" color="#fff" /> : <Text style={[styles.buttonText, customStyle]}>{label}</Text>}
    </TouchableOpacity>
  );
};

export default Button;
