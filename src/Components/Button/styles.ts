import { StyleSheet } from 'react-native';
import { Colors } from '../../Utils';

const styles = StyleSheet.create({
  buttonContainer: {
    flexDirection: 'row',
    width: '100%',
    height: 48,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
    backgroundColor: Colors.Blue,
  },
  buttonText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: Colors.White,
  },
});
export default styles;
