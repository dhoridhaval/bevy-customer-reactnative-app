import { StyleSheet } from 'react-native';
import { Colors } from '../../Utils';

const styles = StyleSheet.create({
  CategoryTitle: {
    textAlign: 'center',
    fontSize: 30,
    color: '#000000',
    fontWeight: '700',
    marginTop: 12,
    marginBottom: 16,
  },
  Container: {
    marginHorizontal: 10,
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  CategoryCard: {
    height: 140,
    marginBottom: 18,
    width: 115,
    borderRadius: 15,
    borderWidth: 1.5,
    borderColor: Colors.GreyG,
  },
  CardInner: {
    height: 105,
    backgroundColor: '#FCFAF8',
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  CardTitle: {
    fontSize: 11,
    marginTop: 5,
    color: '#000000',
    fontWeight: '800',
    textAlign: 'center',
    paddingHorizontal: 1,
  },
});
export default styles;
