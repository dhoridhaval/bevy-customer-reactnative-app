import React from 'react';
import { Image, Text, View } from 'react-native';
import { Dummydata } from '../../Utils/Data';
import style from './styles';

const CategorySection = () => {
  return (
    <View>
      <Text style={style.CategoryTitle}>Categorie</Text>
      <View style={style.Container}>
        {Dummydata.map((item, index) => (
          <View style={style.CategoryCard}>
            <View style={style.CardInner}>
              <Image source={item.uri} />
            </View>
            <Text style={style.CardTitle}>{item.title}</Text>
          </View>
        ))}
      </View>
    </View>
  );
};

export default CategorySection;
