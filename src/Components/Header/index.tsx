import React from 'react';
import { Image, Text,TouchableOpacity, View } from 'react-native';
import { Images } from '../../Utils';
import style from './styles';

const Header = () => {
  return (
    <View>
      <Image source={Images.BEVYLOGO} style={style.bevyIcon} />
      <View style={style.horizontaline} />
      <View style={style.addressContainer}>
        <TouchableOpacity>
          <Image source={Images.LOCATIONICON} style={style.locationIcon} />
        </TouchableOpacity>
        <View style={style.location}>
          <Text style={style.locationTitle}>Via Torino 15, 50032, Milano</Text>
          <Text style={style.locationSubTitle}>
            Prossima Consegna <Text style={style.time}>16:00 - 18:00</Text>
          </Text>
        </View>
        <TouchableOpacity>
          <Image source={Images.EDITICON} style={style.editIcon} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Header;
