import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  bevyIcon: {
    height: 28,
    width: 96,
    marginHorizontal: 16,
    marginTop: 17,
    marginBottom: 15,
  },
  addressContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 16,
    marginTop: 10,
    marginBottom: 12,
  },
  horizontaline: {
    borderBottomColor: '#F4F3F4',
    borderBottomWidth: 1,
  },
  locationIcon: {
    height: 40,
    width: 40,
  },
  location: {
    display: 'flex',
    marginLeft: 11,
    fontSize: 16,
  },
  locationTitle: {
    fontWeight: '600',
    color: '#000000',
  },
  locationSubTitle: {
    fontWeight: '400',
    color: '#6A6A6A',
  },
  time: {
    color: '#0033A1',
  },
  editIcon: {
    height: 24,
    width: 24,
    marginLeft: 67,
  },
});
export default styles;
