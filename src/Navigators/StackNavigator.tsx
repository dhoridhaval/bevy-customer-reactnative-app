import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Login from '../Containers/Login';
import Home from '../Containers/Home';

type RootStackParamList = {
  Login: undefined;
  Home: undefined;
};

const RootStack = createNativeStackNavigator<RootStackParamList>();

const ApplicationNavigator = () => {
  return (
    <NavigationContainer>
      <RootStack.Navigator initialRouteName="Home">
        <RootStack.Screen name="Home" component={Home} />
        {/* <RootStack.Screen name="Login" component={Login} /> */}
      </RootStack.Navigator>
    </NavigationContainer>
  );
};

export default ApplicationNavigator;
