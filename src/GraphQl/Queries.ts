import { gql } from "@apollo/client";

const LOGIN_QUERY = gql`
mutation SignInWithEmailAndPassword(
  $email: String!, 
  $password: String!,
) {
  customerAccessTokenCreate(input: { 
      email: $email, 
      password: $password,
  }) {
      customerAccessToken {
          accessToken
          expiresAt
      }
      customerUserErrors {
          code
          message
      }
  }
}
`
export default LOGIN_QUERY;
