import { StyleSheet } from 'react-native';
import { Colors } from '../../Utils';

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.White,
    justifyContent: 'center',
    paddingHorizontal: 20,
  },
  footerContainer: {
    justifyContent: 'center',
    marginTop: 10,
  },
  buttonView: {
    marginTop: 10,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    color: Colors.Black,
    marginBottom: 20,
  },
  subtitle: {
    fontSize: 15,
    color: Colors.Black,
    marginBottom: 10,
    marginLeft: 5,
  },
  forgotPassword: {
    fontSize: 16,
    fontWeight: 'bold',
    color: Colors.Gray,
    marginLeft: 10,
    textAlign: 'center',
  },
  forgotText: {
    marginTop: 5,
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
    color: Colors.Blue,
    textDecorationLine: 'underline',
  },
  error: {
    color: 'red'
  },
});

export default style;
