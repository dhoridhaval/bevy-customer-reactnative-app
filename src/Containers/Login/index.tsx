import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { useMutation } from '@apollo/client';
import { Button, InputField } from '../../Components';
import { FormikProvider, useFormik } from 'formik';
import { LoginValidation } from '../../Utils';
import style from './style';
import { LOGIN_QUERY } from '../../GraphQl';

const Login = ({ navigation }: any) => {
  const [SignInWithEmailAndPassword, { data, loading, error }] = useMutation(LOGIN_QUERY, {
    errorPolicy: "all",
    onCompleted: (logindata) => {
      if (logindata?.customerAccessTokenCreate.customerAccessToken?.accessToken) {
        navigation.navigate('Home')
      }
    }
  })
  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
    },
    validationSchema: LoginValidation,
    onSubmit: values => {
      try {
        SignInWithEmailAndPassword({
          variables: {
            "email": values.email,
            "password": values.password
          }
        })
      } catch (err) { console.log(err) }
    },
  });
  const { handleSubmit, handleChange, values, errors, touched } = formik;
  console.log('errorr', error)

  return (
    <View style={style.container}>
      <Text style={style.title}>ACCOUNT</Text>

      <Text style={style.subtitle}>Indirizzo Email</Text>

      <FormikProvider value={formik}>
        <InputField
          placeholder={'Enter Your Email'}
          value={values.email}
          onChangeText={handleChange('email')}
          error={Boolean(touched.email && errors.email)}
          errorText={errors.email}
        />
        <Text style={style.subtitle}>Password</Text>
        <InputField
          placeholder={'Enter Your Passeword'}
          value={values.password}
          onChangeText={handleChange('password')}
          error={Boolean(touched.password && errors.password)}
          errorText={errors.password}
        />
        {data && data.customerAccessTokenCreate &&
          data.customerAccessTokenCreate.customerUserErrors &&
          data.customerAccessTokenCreate.customerUserErrors.length > 0 &&
          <Text style={style.error}>{data.customerAccessTokenCreate.customerUserErrors[0].message}</Text>
        }

        <View style={style.buttonView}>
          <Button label={'LOGIN'} onPress={handleSubmit} isLoading={loading} />
        </View>

        <View style={style.footerContainer}>
          <Text style={style.forgotPassword}>
            Hai dimenticato la tua password ? oppure
          </Text>
          <TouchableOpacity>
            <Text style={style.forgotText}>Torna allo shop</Text>
          </TouchableOpacity>
        </View>
      </FormikProvider>
    </View>
  );
};

export default Login;
