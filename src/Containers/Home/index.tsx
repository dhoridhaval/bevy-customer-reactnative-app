import React from 'react';
import { ScrollView, View } from 'react-native';
import { DiscountsBanner } from '../../Components';
import CategorySection from '../../Components/CategorySection';
import Header from '../../Components/Header';
import HeaderCarousel from '../../Components/HeaderCarousel/HeaderCarousel';
import HowToUse from '../../Components/HowToUse';
import style from './style';

const Home = () => {
  return (
    <ScrollView>
      <View style={style.container}>
        <DiscountsBanner />
        <Header />
        <HeaderCarousel />
        <HowToUse />
        <CategorySection />
      </View>
    </ScrollView>
  );
};

export default Home;
