import { StyleSheet } from 'react-native';
import { Colors } from '../../Utils';

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.White,
  },
});

export default style;
