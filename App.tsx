import React from 'react';
import ApplicationNavigator from './src/Navigators/StackNavigator';
import { ApolloClient, InMemoryCache, ApolloProvider } from '@apollo/client';
import {BASE_URL, STOREFRONT_API_TOKEN} from './src/config'

// Initialize Apollo Client
const client = new ApolloClient({
  connectToDevTools: true,
  uri: BASE_URL,
  credentials: 'same-origin',
  headers: {
    'X-Shopify-Storefront-Access-Token': `${STOREFRONT_API_TOKEN}`,
    'Content-Type': 'application/json',
    'accept': 'application/json',
    'Access-Control-Allow-Origin': '*',
  },
  cache: new InMemoryCache()
});

const App = () => {
  return (
    <ApolloProvider client={client}>
      <ApplicationNavigator />
    </ApolloProvider>
  )
};

export default App;
